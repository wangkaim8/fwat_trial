!=====================================================================
!
!               Full Waveform Adjoint Tomography -v1.0
!               ---------------------------------------
!
!     Main historical authors: Kai Wang
!                              Macquarie Uni, Australia
!                            & University of Toronto, Canada
!                           (c) Martch 2020
!
!=====================================================================
!
subroutine run_preprocessing(model,evtset,ievt,simu_type)

  use specfem_par, only: CUSTOM_REAL, MAX_STRING_LEN, OUTPUT_FILES, IIN, network_name, station_name,nrec, nrec_local, &
       seismograms_d, ispec_selected_rec, number_receiver_global, myrank, t0,islice_selected_rec
  use shared_input_parameters, only: NSTEP, DT, NPROC

  use specfem_par_elastic, only: ispec_is_elastic, ELASTIC_SIMULATION
  use specfem_par_acoustic, only: ispec_is_acoustic, ACOUSTIC_SIMULATION
  use fullwave_adjoint_tomo_par
  use my_mpi

  use ma_constants
  use ma_variables
  
  implicit none

  character(len=MAX_STRING_LEN)                   :: model 
  character(len=MAX_STRING_LEN)                   :: evtset 
  character(len=MAX_STRING_LEN)                   :: simu_type 
  integer                                                     :: irec, irec_local, ispec
  integer                                                     :: ievt,icomp

  ! for reading FKtimes
  character(len=256),dimension(:), allocatable :: fk_netwk,fk_stnm
  real(kind=4) , dimension(:), allocatable :: fk_ttp,fk_tb,fk_te 
  integer                                         :: fk_irec,fk_nrec
  real :: dummy,win_tb,win_te
  ! for reading dat in sac format
  logical                                                     :: findfile
  character(len=MAX_STRING_LEN)                             :: datafile,file_prefix0
  double precision, dimension(NDIM)                         :: datarray
  integer                                                   :: npt1,npt1_inp,ier
  double precision                                          :: t01,t01_p,dt1
  ! for rotation
  real(kind=CUSTOM_REAL)                           seismo_syn(3,NSTEP)                           
  ! for preprocessing 
  double precision                                  :: t0_inp,t1_inp,dt_inp
  integer                                           :: win_b, win_e ! for norm in signal window
  double precision, dimension(NDIM)                 :: dat_raw, syn_raw
  double precision, dimension(NDIM)                 :: dat_inp, syn_inp,syn_dt_phydisp, syn_dtw_phydisp
  double precision, dimension(NDIM)                 :: dat_inp_bp, syn_inp_bp
  double precision                                 :: fstart0,fend0
  ! for measurement 
  double precision                                 :: tstart,tend
  integer                                          :: out_imeas
  integer                                          :: NDIM_CUT
  double precision, dimension(NDIM)                 :: adj_syn_all
  character(len=MAX_STRING_LEN)                     :: bandname,adjfile
  integer :: yr,jda,ho,mi        
  integer :: iband
  double precision :: sec,dist,az,baz,slat,slon
  double precision :: baz_all(nrec)
  character(len=10) :: net,sta,chan_dat
  double precision, dimension(NCHI) :: window_chi
  double precision :: tr_chi, am_chi, T_pmax_dat, T_pmax_syn
  ! distribute stations evenly to local proc for measure_adj
  integer                                               :: my_nrec_local
  integer                                               :: nrec_local_max
  ! for collecting SEM synthetics to one large array
  real(kind=CUSTOM_REAL), dimension(nrec,NSTEP,3)  :: glob_sem_disp
  real(kind=CUSTOM_REAL), dimension(:,:,:), allocatable :: sem_disp_loc
  ! Kai added for amplitude scaling of data gather 
  integer :: igood
  real :: avgamp0,avgamp
  ! for calculating STF for TeleFWI
  real(kind=4), dimension(nrec,NSTEP,NRCOMP)  :: glob_dat_tw,glob_syn_tw
  real(kind=4), dimension(nrec,NSTEP)  :: glob_ff
  real(kind=4), dimension(NSTEP)  :: one_seismo_dat,one_seismo_syn
  character(len=256), dimension(nrec)  :: glob_stnm
  real(kind=4), dimension(:,:,:), allocatable :: dat_tw_loc,syn_tw_loc
  real(kind=4), dimension(:,:), allocatable :: ff_loc
  character(len=256), dimension(:), allocatable :: stnm_loc
  real(kind=4)                                  :: ttp(nrec),tb(nrec),te(nrec)
  real(kind=4) , dimension(NSTEP,NRCOMP)  :: stf_array
  real(kind=4), dimension(:), allocatable       :: tmpl
  ! for writing window_chi by one proc
  character(len=256),dimension(nrec) :: glob_file_prefix0
  character(len=256),dimension(nrec) :: glob_net 
  character(len=256),dimension(nrec) :: glob_sta  
  character(len=256),dimension(nrec,NRCOMP) :: glob_chan_dat   
  double precision, dimension(nrec,NRCOMP)  :: glob_tstart
  double precision, dimension(nrec,NRCOMP)  :: glob_tend
  double precision, dimension(nrec,NRCOMP,NCHI)  :: glob_window_chi
  double precision, dimension(nrec,NRCOMP)  :: glob_tr_chi
  double precision, dimension(nrec,NRCOMP)  :: glob_am_chi
  double precision, dimension(nrec,NRCOMP)  :: glob_T_pmax_dat
  double precision, dimension(nrec,NRCOMP)  :: glob_T_pmax_syn
  integer         , dimension(nrec,NRCOMP)  :: glob_imeas
  character(len=256),dimension(:), allocatable :: file_prefix0_loc
  character(len=256),dimension(:), allocatable :: net_loc 
  character(len=256),dimension(:), allocatable :: sta_loc  
  character(len=256),dimension(:,:), allocatable :: chan_dat_loc   
  real(kind=CUSTOM_REAL), dimension(:,:), allocatable  :: tstart_loc
  real(kind=CUSTOM_REAL), dimension(:,:), allocatable  :: tend_loc
  real(kind=CUSTOM_REAL), dimension(:,:,:), allocatable  :: window_chi_loc
  real(kind=CUSTOM_REAL), dimension(:,:), allocatable  :: tr_chi_loc
  real(kind=CUSTOM_REAL), dimension(:,:), allocatable  :: am_chi_loc
  real(kind=CUSTOM_REAL), dimension(:,:), allocatable  :: T_pmax_dat_loc
  real(kind=CUSTOM_REAL), dimension(:,:), allocatable  :: T_pmax_syn_loc
  integer               , dimension(:,:), allocatable  :: imeas_loc 
  integer :: chi_fileid
  ! for communication between procs
  integer                                        :: tag, nsta_irank, irank
  integer,                dimension(:), allocatable       :: irec_global 
  integer                                        :: status(MPI_STATUS_SIZE)
  ! for writing STATIONS_***_ADJOINT
  character(len=MAX_STRING_LEN) :: dummystring, sta_name,net_name
  double precision :: stlat,stlon,stele,stbur
  ! for writing adjoint sources
  integer :: num_adj
  double precision, dimension(3,NDIM)                 :: adj_syn_all_sum
  real(kind=CUSTOM_REAL), dimension(3,NDIM)                 :: adj_arrays_zne
  character(len=MAX_STRING_LEN) :: dat_coord ! ZRT or ZNE
  double precision :: adj_amp_max
  real(kind=CUSTOM_REAL) :: total_misfit,misfit,total_misfit_reduced
  

  !
  if (myrank==0) write(OUT_FWAT_LOG,*) 'This is run_preprocessing ...' 
  if (myrank==0 .and. simu_type =='tele') then
  ! opens FKtimes file
  datafile='src_rec/FKtimes_'//trim(evtid_names(ievt))
  open(unit=IIN,file=trim(datafile),status='old',action='read',iostat=ier)
  if (ier /= 0) call exit_mpi(myrank,'error opening file '//trim(datafile))
    ! reads all stations
  fk_nrec=0
  do
      read(IIN,*,iostat=ier) dummystring,dummystring,dummy
      if (ier /= 0) exit
      fk_nrec=fk_nrec+1
  enddo
  ! close receiver file
  close(IIN)
  allocate(fk_netwk(fk_nrec)) 
  allocate(fk_stnm(fk_nrec)) 
  allocate(fk_ttp(fk_nrec)) 
  allocate(fk_tb(fk_nrec)) 
  allocate(fk_te(fk_nrec)) 
  open(unit=IIN,file=trim(datafile),status='old',action='read',iostat=ier)
  do irec=1,fk_nrec
      if (TW_AFTER.eq.0.) then
         if (irec==1) write(*,*) 'Read TW_BEFORE and TW_AFTER from FKtimes...'
         read(IIN,*,iostat=ier) fk_netwk(irec),fk_stnm(irec),fk_ttp(irec),fk_tb(irec),fk_te(irec)
      else
         if (irec==1) write(*,*) 'Read TW_BEFORE and TW_AFTER from FWAT.PAR'
         read(IIN,*,iostat=ier) fk_netwk(irec),fk_stnm(irec),fk_ttp(irec)
      endif
  enddo
  close(IIN)
  ! find the right ttp among all stations (Note STATIONS_FILTERED might be
  ! less than STATIONS/FKtimes, thus we need to search for the right ttp )
  do irec=1,nrec
     do fk_irec=1,fk_nrec
        if (trim(network_name(irec))==trim(fk_netwk(fk_irec)) .and. &
            trim(station_name(irec))==trim(fk_stnm(fk_irec)) ) then
           ttp(irec)=fk_ttp(fk_irec)
           if (TW_AFTER.eq.0.) then
              tb(irec)=fk_tb(fk_irec)
              te(irec)=fk_te(fk_irec)
           else
              tb(irec)=TW_BEFORE
              te(irec)=TW_AFTER
           endif
           write(*,*)'netwk,stnm,ttp,tb,te=',trim(network_name(irec)),'.',trim(station_name(irec)), &
                                             ttp(irec),tb(irec),te(irec)
           if( (ttp(irec)-t0+te(irec)) > (-t0+(NSTEP-1)*DT) ) then
              write(*,*)'ttp exceed data range'
              stop
           endif
        endif
     enddo
  enddo
  endif ! end of myrank==0
  if (simu_type =='tele') then
     call bcast_all_cr(ttp,nrec)
     call bcast_all_cr(tb,nrec)
     call bcast_all_cr(te,nrec)
  endif 
  !!! WK: delete win_tb win_te in the future
  if (TW_AFTER.eq.0.) then
     win_tb=tb(1)
     win_te=te(1)
  else
     win_tb=TW_BEFORE
     win_te=TW_AFTER
  endif
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  !-------- collect synthetic from different PROCs to one large array   ------------------- 
  call synchronize_all()
  do irank=1,NPROC-1
     if (myrank == 0) then
        nsta_irank=0
        do irec = 1,  nrec
           if (islice_selected_rec(irec) == irank) nsta_irank = nsta_irank + 1
        enddo
        if (nsta_irank > 0) then
           allocate(irec_global(nsta_irank),stat=ier)
           allocate(sem_disp_loc(nsta_irank,NSTEP,3))
           !! data to receive
           irec_local=0
           tag   = 2001 !MPI_ANY_TAG

           call MPI_RECV(sem_disp_loc, nsta_irank*NSTEP*3, MPI_REAL, irank, tag, my_local_mpi_comm_world, status,  ier)
           call MPI_RECV(irec_global, nsta_irank, MPI_INTEGER, irank, tag, my_local_mpi_comm_world, status,  ier)
           do icomp=1,3
              do irec_local = 1, nsta_irank
                 glob_sem_disp(irec_global(irec_local),:,icomp) = sem_disp_loc(irec_local,:, icomp)
              enddo
           enddo
           deallocate(sem_disp_loc)
           deallocate(irec_global)
        endif
     else
        if (myrank == irank .and. nrec_local > 0) then
           !write(*,*)'myrank,irank,nrec_local=',myrank,irank,nrec_local,number_receiver_global
           allocate(irec_global(nrec_local),stat=ier)
           allocate(sem_disp_loc(nrec_local,NSTEP,3))

           do irec_local = 1, nrec_local
              irec_global(irec_local) = number_receiver_global(irec_local)
              !! choose the rigth seismograms_*
              do icomp=1,3
                 sem_disp_loc(irec_local,:,icomp)=seismograms_d(icomp,irec_local,:)
              enddo
           enddo

           tag    = 2001
           call MPI_SEND(sem_disp_loc,  nrec_local*NSTEP*3, MPI_REAL, 0, tag, my_local_mpi_comm_world, ier)
           call MPI_SEND(irec_global, nrec_local, MPI_INTEGER, 0, tag, my_local_mpi_comm_world, ier)
           deallocate(sem_disp_loc)
           deallocate(irec_global)

        endif

     endif
     ! not sure if need this sync
     call synchronize_all()
  enddo ! end loop irank
  call synchronize_all()
  call bcast_all_cr(glob_sem_disp,nrec*NSTEP*3)
  if (myrank==0) write(*,*) 'Now distridute ',nrec,' stations to ',NPROC,'procs'
  nrec_local_max=ceiling(real(nrec)/real(NPROC))
  my_nrec_local=0
  do irec_local = 1, nrec_local_max
     irec = myrank*nrec_local_max+irec_local
     if(irec<=nrec) my_nrec_local=my_nrec_local+1
  enddo
  write(*,*) 'Rank ',myrank,' has ',my_nrec_local,' stations for measure_adj '
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  do iband=1,NUM_FILTER
    fstart0=1./LONG_P(iband)
    fend0=1./SHORT_P(iband)
    write(bandname,'(a1,i3.3,a2,i3.3)') 'T',SHORT_P(iband),'_T',LONG_P(iband)
    !********* 
    if (myrank==0) chi_fileid=30+iband
    if (myrank==0.and.ievt==1) then
      open(chi_fileid,file='misfits/'//trim(model)//'.'//trim(evtset)//'_'//trim(bandname)&
                 //'_window_chi',status='unknown',iostat=ier)
    endif
    glob_tstart=0.
    glob_tend=0.
    glob_window_chi=0.
    glob_tr_chi=0.
    glob_am_chi=0.
    glob_T_pmax_dat=0.
    glob_T_pmax_syn=0.
    glob_imeas=0
    adj_syn_all_sum=0.
    baz_all=0.
    !*********
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    !=================Calculate STF for TeleFWI =======================================
    glob_dat_tw=0.
    glob_syn_tw=0.
    glob_ff=0.
   
    if(my_nrec_local > 0.and.trim(simu_type)=="tele") then
    !write(*,*) "Rank ",myrank,' has ',nrec_local,' receivers:'
       write(*,*) ' This is a trial version of FWAT for the teleFWI with limited functionality!' 
       write(*,*) ' Please email Kai Wang (wangkaim8@gmail.com) to access the full waversion of FWAT.' 
    endif ! end nrec >0
    
    !=================end of meas_adj for TeleFWI =======================================
    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    !======================= For noise FWI ===============================================
    if(my_nrec_local > 0 .and. simu_type=="noise") then
    !write(*,*) "Rank ",myrank,' has ',nrec_local,' receivers:'
       write(*,*) ' This is a trial version of FWAT for the noiseFWI with limited functionality!' 
       write(*,*) ' Please email Kai Wang (wangkaim8@gmail.com) to access the full waversion of FWAT.' 
    endif ! end nrec >0 for noise
  !====================== end of meas_adj noise FWI   ===========================================
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    total_misfit_reduced=0.
    call sum_all_all_cr(total_misfit, total_misfit_reduced)
    if(myrank==0) write(*,*) 'Total misfit is ',total_misfit_reduced
    if(myrank==0) write(OUT_FWAT_LOG,*) 'Total misfit of this event is ',total_misfit_reduced

  !======================================================================
  enddo ! end loop iband
  
  !***************** sum adjoint sources over bands **********************
  if (myrank==0) write(*,*) 'sum and rotate adjoint sources to ZNE ...'
  call synchronize_all()
  if(myrank==0) write(*,*) 'Finished preprocessing here.'
  
end subroutine run_preprocessing
